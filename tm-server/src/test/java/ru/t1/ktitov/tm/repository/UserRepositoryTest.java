package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.List;

import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    private Connection getConnection() {
        return connectionService.getConnection();
    }

    private void compareUsers(@NotNull final User user1, @NotNull final User user2) {
        Assert.assertEquals(user1.getId(), user2.getId());
        Assert.assertEquals(user1.getLogin(), user2.getLogin());
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
        Assert.assertEquals(user1.getEmail(), user2.getEmail());
        Assert.assertEquals(user1.getFirstName(), user2.getFirstName());
        Assert.assertEquals(user1.getLastName(), user2.getLastName());
        Assert.assertEquals(user1.getMiddleName(), user2.getMiddleName());
        Assert.assertEquals(user1.getRole(), user2.getRole());
        Assert.assertEquals(user1.getLocked(), user2.getLocked());
    }

    private void compareUsers(
            @NotNull final List<User> userList1,
            @NotNull final List<User> userList2) {
        Assert.assertEquals(userList1.size(), userList2.size());
        for (int i = 0; i < userList1.size(); i++) {
            compareUsers(userList1.get(i), userList2.get(i));
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.delete(USER1.getId());
            repository.delete(USER2.getId());
            repository.delete(ADMIN3.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            compareUsers(USER1, repository.findAll().get(0));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER_LIST1);
            Assert.assertEquals(3, repository.getSize());
            compareUsers(USER_LIST1, repository.findAll());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void set() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            repository.set(USER_LIST2);
            compareUsers(USER_LIST2, repository.findAll());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            Assert.assertEquals(3, repository.getSize());
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            compareUsers(USER_LIST1, repository.findAll());
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            Assert.assertTrue(repository.existsById(USER1.getId()));
            Assert.assertTrue(repository.existsById(USER2.getId()));
            Assert.assertTrue(repository.existsById(ADMIN3.getId()));
            Assert.assertFalse(repository.existsById(USER4.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            compareUsers(USER1, repository.findOneById(USER1.getId()));
            compareUsers(USER2, repository.findOneById(USER2.getId()));
            Assert.assertFalse(repository.existsById(USER4.getId()));
            compareUsers(ADMIN3, repository.findOneById(ADMIN3.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            Assert.assertEquals(3, repository.getSize());
            repository.remove(USER1);
            Assert.assertEquals(2, repository.getSize());
            repository.remove(USER4);
            Assert.assertEquals(2, repository.getSize());
            repository.removeById(USER2.getId());
            Assert.assertEquals(1, repository.getSize());
            compareUsers(ADMIN3, repository.findAll().get(0));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            compareUsers(USER1, repository.findByLogin("user1"));
            compareUsers(USER2, repository.findByLogin("user2"));
            compareUsers(ADMIN3, repository.findByLogin("user3"));
            Assert.assertNull(repository.findByLogin("admin1"));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            compareUsers(USER1, repository.findByEmail("user1@gmail.com"));
            compareUsers(USER2, repository.findByEmail("user2@gmail.com"));
            compareUsers(ADMIN3, repository.findByEmail("user3@gmail.com"));
            Assert.assertNull(repository.findByEmail("admin1@gmail.com"));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void isLoginExist() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            Assert.assertTrue(repository.isLoginExist("user1"));
            Assert.assertTrue(repository.isLoginExist("user2"));
            Assert.assertTrue(repository.isLoginExist("user3"));
            Assert.assertFalse(repository.isLoginExist("admin1"));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void isEmailExist() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST1);
            Assert.assertTrue(repository.isEmailExist("user1@gmail.com"));
            Assert.assertTrue(repository.isEmailExist("user2@gmail.com"));
            Assert.assertTrue(repository.isEmailExist("user3@gmail.com"));
            Assert.assertFalse(repository.isEmailExist("admin1@gmail.com"));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
