package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    private Connection getConnection() {
        return connectionService.getConnection();
    }

    private void compareProjects(@NotNull final Project project1, @NotNull final Project project2) {
        Assert.assertEquals(project1.getId(), project2.getId());
        Assert.assertEquals(project1.getName(), project2.getName());
        Assert.assertEquals(project1.getDescription(), project2.getDescription());
        Assert.assertEquals(project1.getStatus(), project2.getStatus());
        Assert.assertEquals(project1.getUserId(), project2.getUserId());
        Assert.assertEquals(project1.getCreated(), project2.getCreated());
    }

    private void compareProjects(
            @NotNull final List<Project> projectList1,
            @NotNull final List<Project> projectList2) {
        Assert.assertEquals(projectList1.size(), projectList2.size());
        for (int i = 0; i < projectList1.size(); i++) {
            compareProjects(projectList1.get(i), projectList2.get(i));
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.clear(USER1.getId());
            repository.clear(USER2.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_PROJECT1);
            compareProjects(USER1_PROJECT1, repository.findAll().get(0));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_PROJECT_LIST);
            Assert.assertEquals(3, repository.getSize());
            compareProjects(USER1_PROJECT_LIST, repository.findAll());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1.getId(), USER1_PROJECT1);
            compareProjects(USER1_PROJECT1, repository.findAll().get(0));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.add(USER1.getId(), USER1_PROJECT1);
            compareProjects(USER1_PROJECT1, repository.findAll().get(0));
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.add(USER1_PROJECT_LIST);
            compareProjects(USER1_PROJECT_LIST, repository.findAll());
            repository.clear(USER2.getId());
            Assert.assertFalse(repository.findAll().isEmpty());
            repository.clear(USER1.getId());
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_PROJECT1);
            repository.clear(USER2.getId());
            compareProjects(USER1_PROJECT1, repository.findAll().get(0));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.add(USER1USER2_PROJECT_LIST);
            compareProjects(USER1USER2_PROJECT_LIST, repository.findAll());
            compareProjects(USER1_PROJECT_LIST, repository.findAll(USER1.getId()));
            compareProjects(USER2_PROJECT_LIST, repository.findAll(USER2.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.add(USER1_PROJECT_LIST);
            compareProjects(USER1_PROJECT1, repository.findOneById(USER1_PROJECT1.getId()));
            Assert.assertNull(repository.findOneById(USER2.getId(), USER1_PROJECT1.getId()));
            compareProjects(USER1_PROJECT1, repository.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
            Assert.assertTrue(repository.existsById(USER1_PROJECT1.getId()));
            Assert.assertFalse(repository.existsById(USER2.getId(), USER1_PROJECT1.getId()));
            Assert.assertTrue(repository.existsById(USER1.getId(), USER1_PROJECT1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.add(USER1_PROJECT_LIST);
            repository.remove(USER1_PROJECT1);
            Assert.assertEquals(2, repository.getSize());
            repository.removeById(USER1_PROJECT2.getId());
            Assert.assertEquals(1, repository.getSize());
            compareProjects(USER1_PROJECT3, repository.findAll().get(0));
            repository.clear();
            repository.add(USER1_PROJECT_LIST);
            Assert.assertEquals(3, repository.getSize());
            repository.removeById(USER2.getId(), USER1_PROJECT1.getId());
            Assert.assertEquals(3, repository.getSize());
            repository.removeById(USER1.getId(), USER1_PROJECT1.getId());
            repository.removeById(USER1.getId(), USER1_PROJECT2.getId());
            compareProjects(USER1_PROJECT3, repository.findAll().get(0));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
