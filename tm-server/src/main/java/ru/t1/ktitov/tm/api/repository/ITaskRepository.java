package ru.t1.ktitov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task update(@Nullable Task project);

}
