package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.IRepository;
import ru.t1.ktitov.tm.comparator.CreatedComparator;
import ru.t1.ktitov.tm.comparator.NameComparator;
import ru.t1.ktitov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    @NotNull
    private final String tableName;

    protected AbstractRepository(
            @NotNull final Connection connection,
            @NotNull final String tableName
    ) {
        this.connection = connection;
        this.tableName = tableName;
    }

    @NotNull
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    @Nullable
    public abstract M add(@Nullable final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        @NotNull final List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("TRUNCATE TABLE %s", tableName);
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", tableName);
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return null;
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s ORDER BY %s",
                tableName, getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        return "status";
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE ID = ? LIMIT 1", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) return fetch(resultSet);
            return null;
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s", tableName);
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) return resultSet.getInt(1);
            return 0;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE ID = ?", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

}
