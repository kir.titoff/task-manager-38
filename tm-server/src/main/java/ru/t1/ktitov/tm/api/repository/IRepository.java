package ru.t1.ktitov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    @Nullable
    M add(@Nullable M model);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Comparator comparator);

    boolean existsById(@Nullable String id);

    M findOneById(@Nullable String id);

    int getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String id);

}
