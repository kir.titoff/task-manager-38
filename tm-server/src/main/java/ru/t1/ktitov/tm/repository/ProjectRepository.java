package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection, "tm_project");
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable final Project model) {
        if (model == null) return null;
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, CREATED, NAME, DESCRIPTION, STATUS, USER_ID) " +
                        "VALUES (?, ?, ?, ?, ?, ?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, Status.NOT_STARTED.getDisplayName());
            statement.setString(6, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public Project add(@Nullable final String userId, @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) return null;
        if (model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("ID"));
        project.setCreated(row.getTimestamp("CREATED"));
        project.setName(row.getString("NAME"));
        project.setDescription(row.getString("DESCRIPTION"));
        @Nullable final Status status = Status.toStatus(row.getString("STATUS"));
        if (status == null)
            project.setStatus(Status.NOT_STARTED);
        else project.setStatus(status);
        project.setUserId(row.getString("USER_ID"));
        return project;
    }


    @Nullable
    @Override
    @SneakyThrows
    public Project update(@Nullable Project project) {
        if (project == null) return null;
        @NotNull final String sql = String.format(
                "UPDATE %s SET NAME = ?, DESCRIPTION = ?, STATUS = ? WHERE ID = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().getDisplayName());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

}
