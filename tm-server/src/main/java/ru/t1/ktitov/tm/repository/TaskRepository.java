package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection, "tm_task");
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task model) {
        if (model == null) return null;
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, CREATED, NAME, DESCRIPTION, STATUS, USER_ID, PROJECT_ID) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, Status.NOT_STARTED.getDisplayName());
            statement.setString(6, model.getUserId());
            statement.setString(7, model.getProjectId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public Task add(@Nullable final String userId, @Nullable final Task model) {
        if (userId == null || userId.isEmpty()) return null;
        if (model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("ID"));
        task.setCreated(row.getTimestamp("CREATED"));
        task.setName(row.getString("NAME"));
        task.setDescription(row.getString("DESCRIPTION"));
        @Nullable final Status status = Status.toStatus(row.getString("STATUS"));
        if (status == null)
            task.setStatus(Status.NOT_STARTED);
        else task.setStatus(status);
        task.setUserId(row.getString("USER_ID"));
        task.setProjectId(row.getString("PROJECT_ID"));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE USER_ID = ? AND PROJECT_ID = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
                result.add(fetch(resultSet));
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task update(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final String sql = String.format(
                "UPDATE %s SET NAME = ?, DESCRIPTION = ?, STATUS = ?, " +
                        "PROJECT_ID = ? WHERE ID = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().getDisplayName());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

}
