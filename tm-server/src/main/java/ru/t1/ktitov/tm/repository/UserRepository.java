package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection, "tm_user");
    }

    @Nullable
    @Override
    @SneakyThrows
    public User add(@Nullable final User model) {
        if (model == null) return null;
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, LOGIN, PASSWORD, FST_NAME, LAST_NAME, MID_NAME, EMAIL, ROLE, LOCKED) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getLogin());
            statement.setString(3, model.getPasswordHash());
            statement.setString(4, model.getFirstName());
            statement.setString(5, model.getLastName());
            statement.setString(6, model.getMiddleName());
            statement.setString(7, model.getEmail());
            statement.setString(8, model.getRole().getDisplayName());
            statement.setBoolean(9, model.getLocked());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("ID"));
        user.setLogin(row.getString("LOGIN"));
        user.setPasswordHash(row.getString("PASSWORD"));
        user.setFirstName(row.getString("FST_NAME"));
        user.setLastName(row.getString("LAST_NAME"));
        user.setMiddleName(row.getString("MID_NAME"));
        user.setEmail(row.getString("EMAIL"));
        @Nullable final Role role = Role.toRole(row.getString("ROLE"));
        if (role == null)
            user.setRole(Role.USUAL);
        else user.setRole(role);
        user.setLocked(row.getBoolean("LOCKED"));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User update(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final String sql = String.format(
                "UPDATE %s SET LOGIN = ?, PASSWORD = ?, EMAIL = ?, FST_NAME = ?, LAST_NAME = ?, " +
                        "MID_NAME = ?, ROLE = ?, LOCKED = ? WHERE ID = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getRole().getDisplayName());
            statement.setBoolean(8, user.getLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE LOGIN = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) return fetch(resultSet);
            return null;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE EMAIL = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) return fetch(resultSet);
            return null;
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    public void delete(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE ID = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
    }

}
