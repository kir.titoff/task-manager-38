package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.ISessionRepository;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection, "tm_session");
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session add(@Nullable final Session model) {
        if (model == null) return null;
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, CREATED, ROLE, USER_ID) " +
                        "VALUES (?, ?, ?, ?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getDate().getTime()));
            statement.setString(3, model.getRole().getDisplayName());
            statement.setString(4, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session add(@Nullable final String userId, @Nullable final Session model) {
        if (userId == null || userId.isEmpty()) return null;
        if (model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("ID"));
        session.setDate(row.getTimestamp("CREATED"));
        session.setRole(Role.toRole(row.getString("ROLE")));
        session.setUserId(row.getString("USER_ID"));
        return session;
    }

}
