package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    Project removeProjectById(@Nullable String userId, @Nullable String projectId);

}
