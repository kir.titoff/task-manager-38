package ru.t1.ktitov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    Project update(@Nullable Project project);

}
