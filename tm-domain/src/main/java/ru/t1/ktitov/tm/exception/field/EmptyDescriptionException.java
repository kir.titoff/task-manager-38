package ru.t1.ktitov.tm.exception.field;

public final class EmptyDescriptionException extends AbstractFieldException {

    public EmptyDescriptionException() {
        super("Error! Description is empty.");
    }

}
