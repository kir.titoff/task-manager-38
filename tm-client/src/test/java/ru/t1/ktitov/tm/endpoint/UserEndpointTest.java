package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.ktitov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.ktitov.tm.dto.request.user.*;
import ru.t1.ktitov.tm.marker.IntegrationCategory;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public final class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String userToken;

    @Nullable
    private String user2Token;

    @Nullable
    private String adminToken;

    @Nullable
    private User userCreate(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        return userEndpoint.registryUser(request).getUser();
    }

    private void userRemove(@NotNull final String login) {
        try {
            @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
            request.setLogin(login);
            userEndpoint.removeUser(request);
        } catch (@NotNull final Exception e) {
        }
    }

    @Nullable
    private String userLogin(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return authEndpoint.login(request).getToken();
    }

    private void userLogout(@Nullable final String token) {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        authEndpoint.logout(request);
    }

    @Before
    public void setUp() {
        adminToken = userLogin("admin", "admin");
        userCreate("test1", "test1", "test1@gmail.com");
        userCreate("test2", "test2", "test2@gmail.com");
        userToken = userLogin("test1", "test1");
        user2Token = userLogin("test2", "test2");
    }

    @After
    public void tearDown() {
        userLogout(userToken);
        userLogout(user2Token);
        userRemove("test1");
        userRemove("test2");
        userLogout(adminToken);
    }

    @Test(expected = RuntimeException.class)
    public void test01AccessDeniedNoToken() {
        @Nullable final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest();
        projectCreateRequest.setName("name");
        projectCreateRequest.setDescription("description");
        projectEndpoint.createProject(projectCreateRequest);
    }

    @Test(expected = RuntimeException.class)
    public void test02AccessDeniedTokenNotFound() {
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
        @Nullable final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName("name");
        projectCreateRequest.setDescription("description");
        projectEndpoint.createProject(projectCreateRequest);
    }

    @Test
    public void test03UserLogin() {
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(user2Token);
        authEndpoint.logout(requestLogout);
        @Nullable final String token = userLogin("test2", "test2");
        Assert.assertNotNull(token);
        @Nullable final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("name");
        projectCreateRequest.setDescription("description");
        projectEndpoint.createProject(projectCreateRequest);
    }

    @Test(expected = RuntimeException.class)
    public void test04LockUserAccessDenied() {
        @NotNull final UserLockRequest request = new UserLockRequest(user2Token);
        request.setLogin("test1");
        userEndpoint.lockUser(request);
    }

    @Test(expected = RuntimeException.class)
    public void test05LockUser() {
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken);
        request.setLogin("test1");
        userEndpoint.lockUser(request);
        userLogin("test1", "test1");
    }

    @Test
    public void test06UnlockUser() {
        @NotNull final UserLockRequest requestLk = new UserLockRequest(adminToken);
        requestLk.setLogin("test1");
        userEndpoint.lockUser(requestLk);
        @NotNull final UserUnlockRequest requestUnlk = new UserUnlockRequest(adminToken);
        requestUnlk.setLogin("test1");
        userEndpoint.unlockUser(requestUnlk);
        userLogin("test1", "test1");
    }

    @Test(expected = RuntimeException.class)
    public void test07RemoveUser() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin("test2");
        userEndpoint.removeUser(request);
        userLogin("test2", "test2");
    }

    @Test
    public void test08RegistryUser() {
        @NotNull final UserRemoveRequest requestRemove = new UserRemoveRequest(adminToken);
        requestRemove.setLogin("test2");
        userEndpoint.removeUser(requestRemove);
        @NotNull final UserRegistryRequest requestReg = new UserRegistryRequest(adminToken);
        requestReg.setLogin("test2");
        requestReg.setPassword("test2");
        requestReg.setEmail("test2@test.ru");
        userEndpoint.registryUser(requestReg);
        userLogin("test2", "test2");
    }

    @Test
    public void test09Profile() {
        @NotNull final UserProfileRequest request = new UserProfileRequest(user2Token);
        @Nullable final User user = userEndpoint.profile(request).getUser();
        Assert.assertEquals("test2", user.getLogin());
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        Assert.assertNull(user.getLastName());
    }

    @Test
    public void test10UpdateUserProfile() {
        @NotNull final UserUpdateProfileRequest requestUpd = new UserUpdateProfileRequest(user2Token);
        requestUpd.setFirstName("fstName");
        requestUpd.setMiddleName("midName");
        requestUpd.setLastName("lastName");
        userEndpoint.updateUserProfile(requestUpd);
        @NotNull final UserProfileRequest requestProfile = new UserProfileRequest(user2Token);
        @Nullable final User user = userEndpoint.profile(requestProfile).getUser();
        Assert.assertEquals("fstName", user.getFirstName());
        Assert.assertEquals("midName", user.getMiddleName());
        Assert.assertEquals("lastName", user.getLastName());
    }

    @Test(expected = RuntimeException.class)
    public void test11ChangeUserPasswordError() {
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(user2Token);
        request.setPassword("new_password");
        userEndpoint.changeUserPassword(request);
        userLogin("test2", "test2");
    }

    @Test
    public void test12ChangeUserPassword() {
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(user2Token);
        request.setPassword("new_password");
        userEndpoint.changeUserPassword(request);
        userLogin("test2", "new_password");
    }

}
